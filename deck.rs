use std::collections::LinkedList;
use std::fmt;

#[derive(Clone,Debug)]
pub struct Suit
{
    id:u32,
    name:String,
}

#[derive(Clone,Debug)]
pub struct Rank
{
    id:u32,
    name:String,
    value:u32,
}

#[derive(Clone,Debug)]
pub struct Card
{
    suit:Suit,
    rank:Rank,
}

impl Suit
{
    pub fn new(id:u32,name:&str) -> Suit
    {
        Suit{id:id,name:name.to_string()}
    }
}

impl Rank
{
    pub fn new(id:u32,name:&str,value:u32) -> Rank
    {
        Rank{id:id,name:name.to_string(),value:value}
    }
}

impl Card
{
    pub fn new(suit:Suit,rank:Rank) -> Card
    {
        Card{suit:suit,rank:rank}
    }
}

type Deck = LinkedList<Card>;

trait DeckConstruct
{
    fn newdeck(suit:[Suit;4],rank:[Rank;13]) -> Deck;
    fn deck()->Deck;
}

impl DeckConstruct for Deck
{
    fn newdeck(suit:[Suit;4],rank:[Rank;13]) -> Deck
    {    
        let mut deck = LinkedList::<Card>::new();

        for s in suit.iter()
        {
            for r in rank.iter()
            {
                deck.push_back(Card::new(s.clone(),r.clone()));
            }
        }

        deck
    }

    fn deck()->Deck
    {
        let suit = 
            [
            Suit::new(0u32,"Club"),Suit::new(1u32,"Diamon"),
            Suit::new(2u32,"Heart"),Suit::new(0u32,"Spade")
            ];
        let rank = 
            [
            Rank::new(0u32,"A",1u32),Rank::new(1u32,"2",2u32),
            Rank::new(2u32,"3",3u32),Rank::new(3u32,"4",4u32),
            Rank::new(4u32,"5",5u32),Rank::new(5u32,"6",6u32),
            Rank::new(6u32,"7",7u32),Rank::new(7u32,"8",8u32),
            Rank::new(8u32,"9",9u32),Rank::new(9u32,"10",10u32),
            Rank::new(10u32,"J",11u32),Rank::new(11u32,"Q",12u32),
            Rank::new(12u32,"K",13u32)
            ];

        Self::newdeck(suit,rank)

    }
}
impl fmt::Display for Suit {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_fmt(format_args!("{}", self.name))
    }
}
impl fmt::Display for Rank {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_fmt(format_args!("{}", self.name))
    }
}
pub fn showdeck(deck:&Deck)
{
    for i in deck.iter()
    {
        print!("[{}({})] ",i.suit,i.rank);
    }
    println!("");
}

fn main()
{
    showdeck(&Deck::deck());
}
