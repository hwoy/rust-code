fn main()
{
    let odd = (0u32..).map(|x| 2*x+1);
    let even = (0u32..).map(|x| 2*x);

    for i in odd.take(10){
        println!("{}",i);
    }
}
