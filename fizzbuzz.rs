fn fizzbuzz(num:u32) -> String
{
    if num%15==0 { "FizzBuzz".to_string() }
    else if num%3==0 { "Fizz".to_string() }
    else if num%5==0 { "Buzz".to_string() }
    else { num.to_string() }
}

fn main()
{
    for i in (1..=100u32).map(|x| fizzbuzz(x))
    {
        println!("{}",i);
    }
}
