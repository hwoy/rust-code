use std::ops::{Mul,Rem,RangeFrom};

pub fn isprime<T>(num:T) -> bool
where T:Copy+From<u8>+Ord+PartialEq+Mul<Output=T>+Rem<Output=T>,
      RangeFrom<T>:Iterator<Item=T>,
{
    if num < 2u8.into()
    {
        return false;
    }

    (2u8.into()..).take_while(|&x| x*x<=num).all(|x| num%x != 0u8.into())
}
