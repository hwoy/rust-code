fn isprime(num:u32)->bool
{
    if num<2
    {
        return false;
    }

    for i in (2..).take_while(|&x| (x*x<=num))
    {
        if num%i==0
        {
            return false;
        }
    }

    return true;

}

fn main()
{
    let range = 1..=10000000;
    println!("============== Prime number between {}-{} ==============",range.start(),range.end());
    for i in range.filter(|&x| isprime(x))
    {
        println!("{}",i);
    }

}
