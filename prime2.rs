use std::ops::{Mul,Rem,AddAssign};

trait Prime
{
    fn isprime(self) -> bool;
}


impl<T> Prime for T
where T:Ord+PartialEq+Mul<Output=Self>+Rem<Output=Self>+AddAssign+From<u8>+Copy,
      {
          fn isprime(self) -> bool
          {
              if self < 2u8.into()
              {
                  return false;
              }

              let mut i = 2u8.into();
              while i*i<=self
              {
                  if self%i == 0u8.into()
                  {
                      return false;
                  }
                  i+=1u8.into();
              }

              return true;
          }

      }

fn main()
{
    let prime = (1u32..).filter(|&x| x.isprime());

    println!("============================================");

    for i in prime.clone().take_while(|&x| x>=1 && x<=100)
    {
        println!("{}",i);
    }

    println!("===========================================");
    for i in prime.take(100)
    {
        println!("{}",i);
    }
}
