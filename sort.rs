fn sort<T:Ord,F:Fn(&T,&T)->bool>(s:&mut [T],f:F)
{
    let mut pivot;
    for i in  (1..s.len()).rev()
    {
        pivot = 0usize;
        for j in 1..=i
        {
            if f(&s[pivot],&s[j])
            {
                pivot=j;
            }
        }

        s.swap(pivot,i);
    }
}

fn main()
{
    let s = &mut [1,5,8,3,-7,6];

    sort(s,|a,b| a<b);

    println!("{:#?}",s);

}
